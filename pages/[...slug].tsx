import dynamic from 'next/dynamic'


function randomChoice(arr: any) {
    return arr[Math.floor(arr.length * Math.random())];
}


const CMSPage = () => {
    const page = randomChoice(["foo", "bar"]);
    console.log(page)
    let DynamicComponent;
    if (page === "foo") {
        DynamicComponent = dynamic(() => import('../components/foo'))
    } else {
        DynamicComponent = dynamic(() => import('../components/bar'))
    }
    return <DynamicComponent />
}

export default CMSPage;