import React from 'react';
import Link from 'next/link'

const HomePage = () => {
  return (
    <>
      <h1>links!!</h1>
      <div>
        <ul>
          <li><Link href="/[pid]" as="/post/abc">Foo</Link></li>
          <li><Link href="/the-bar">Bar</Link></li>
        </ul>
      </div>
    </>
  );
};

export default HomePage;
